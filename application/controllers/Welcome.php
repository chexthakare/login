<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('WelcomeModel');
    }

	public function index()
	{
		$data['countries'] = $this->WelcomeModel->getCountries();
		$this->load->view('welcome_message',$data);
	}

    /* login */ 
	public function login()
	{
		$this->load->view('login');
	}

    /* register */ 
	public function register()
	{
		$username = $this->input->post('username');
		$email = $this->input->post('email');

		if($this->WelcomeModel->checkuname($username))
		{
			$this->session->set_flashdata('message', 'This Username is Already Taken!');
            redirect('welcome');		
		}
		elseif($this->WelcomeModel->checkemail($email)){

			$this->session->set_flashdata('message', 'This Email is Already Taken!');
            redirect('welcome');	
		}
		else
		{
			$countryid = $this->input->post('country');
            $stateid = $this->input->post('state');
            $cityid = $this->input->post('city');

            $countryName = $this->WelcomeModel->getCountryName($countryid);
            $countryName = $countryName->name;
            $stateName = $this->WelcomeModel->getStateName($stateid);
            $stateName = $stateName->name;
            $cityName = $this->WelcomeModel->getCityName($cityid);
            $cityName = $cityName->name;

            $data = array(
        		'fname' => $this->input->post('fname'),
        		'lname' => $this->input->post('lname'),
        		'gender' => $this->input->post('gender'),
        		'email' => $this->input->post('email'),
        		'empdob' => $this->input->post('empdob'),
                'country' => $countryName,
                'state' => $stateName,
                'city' => $cityName,
        		'username' => $this->input->post('username'),
        		'password' => $this->input->post('password'),
        		
        		);
        	$result = $this->WelcomeModel->register($data);
        	if(!$result){
	        	$this->session->set_flashdata('message', 'Record Not Added');
	            redirect('welcome');
        	}
        	else
        	{
        		$this->session->set_flashdata('message', 'Record Added!');
	            redirect('welcome/login');
        	}
		}
	}

    /* get countries */ 
	public function getCountries(){
        $data['countries'] = $this->WelcomeModel->getCountries();
        $this->load->view('welcome_message', $data);
    }
    
    /* get states */    
    public function getStates(){
        $states = array();
        $country_id = $this->input->post('country_id');
        if($country_id){
            $con['conditions'] = array('country_id'=>$country_id);
            $states = $this->WelcomeModel->getStates($con);
        }
        echo json_encode($states);
    }

    /* get cities */ 
    public function getCities(){
        $cities = array();
        $state_id = $this->input->post('state_id');
        if($state_id){
            $con['conditions'] = array('state_id'=>$state_id);
            $cities = $this->WelcomeModel->getCities($con);
        }
        echo json_encode($cities);
    }

    /* check valid user */ 
    public function logcheck(){
        $username = $this->input->post("username");
        $password = $this->input->post("password");

        $result  = $this->WelcomeModel->selectUser($username, $password);
            if ($result > 0) //active user record is present
            {
                 //set the session variables
            	$_SESSION['username'] = $username;
                $this->session->set_flashdata('message', 'Successfully Login.');
                $data['employee'] = $this->WelcomeModel->display();
				$this->load->view('emplist',$data);
                // $this->load->view('welcome',$username);
            }
            else
            {
                 $this->session->set_flashdata('message', 'Incorrect Login');
                 redirect('welcome/login');
            } 
    }

    /* edit employee */ 
    public function edit($employeeid)
	{
		$data['editemp'] = $this->WelcomeModel->edit($employeeid);
		$data['countries'] = $this->WelcomeModel->getCountries();
		$this->load->view('editemp',$data);
		
	}

    /* update employee record */ 
	public function update(){
		$this->WelcomeModel->update();
		$data['employee'] = $this->WelcomeModel->display();
		$data['countries'] = $this->WelcomeModel->getCountries();
		$this->load->view('emplist',$data);
    }

    /* display employee list */ 
    public function emplist()
	{
		$data['employee'] = $this->WelcomeModel->display();
		$this->load->view('emplist',$data);
	}

    /* delete */ 
	public function delete($employeeid)
    {
        $this->WelcomeModel->delete($employeeid);
        $data['employee'] = $this->WelcomeModel->display();
        $this->load->view('emplist',$data);
        
    }

    /* function for logout */ 
    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->set_flashdata('message', 'Successfully Logout.');
        redirect('welcome/login');
        $this->session->set_userdata(array('username'=>$username));

    }








}
