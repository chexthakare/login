<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
</head>
<body>

<div id="container">
	<h1>Employee List! Welcome <?php echo $this->session->userdata('username'); ?></h1>

	<div id="body">
		<div class="container" id="empdata">
	      <div class="row">
	        <div class="col-sm-12 ml-auto mr-auto m-auto" id="hodm_table">
	          <table class="table table-striped table-hover table-responsive">
	            <tr>
	            <thead>
	              <td>First Name</td>
	              <td>Last Name</td>
	              <td>Email Name</td>
	              <td>Birth Date</td>
	              <td>Username</td>
	              <td>Country</td>
	              <td>State</td>
	              <td>Ciry</td>
	              <td>Action</td>
	              <td>Delete</td>
	            </thead>
	            </tr>
	            <tr>
	              <?php foreach ($employee as $value) { ?>
	            <tbody>
	              <td><?php echo $value['fname'];?></td>
	              <td><?php echo $value['lname'];?></td>
	              <td><?php echo $value['email'];?></td>
	              <td><?php echo $value['empdob'];?></td>
	              <td><?php echo $value['username'];?></td>
	              <td><?php echo $value['country'];?></td>
	              <td><?php echo $value['state'];?></td>
	              <td><?php echo $value['city'];?></td>
	              <td><a href="<?php echo site_url('edit/'.$value['employeeid']);?>">Edit</a></td>
	              <td><?=anchor("delete/".$value['employeeid'],"Delete",array('onclick' => "return confirm('Do you want delete this record')"))?></td>
	            </tbody>
	            <?php  } ?>
	            </tr>
	          </table>
	        </div>
	      </div>
	    </div>
	</div>

	<p class="footer">If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>

	<p class="footer">
		<h4><?php echo anchor('logout','Logout');?></h4>
	</p>
</div>

</body>
</html>