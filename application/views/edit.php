<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   ?><!DOCTYPE html>
<html lang="en">
   <head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.12.0.min.js"></script>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


   </head>
   <body>
      </br></br></br>
      <div class="container">
         <div class="row">
            <div class="col-sm-6 ml-auto mr-auto m-auto">
               <div class="card mt-5">
                  <h5 class="card-header"></h5>
                  <div class="card-body">
                     <?php echo form_open('welcome/update', array('id' => 'form2', 'role' => 'form'));?>
                      <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                              <input type="text" class="form-control" name="fname" value="<?php echo $editemp->fname ;?>" id="input-fname">
                              <input type="hidden" class="form-control" name="employeeid" value="<?php echo $editemp->employeeid ;?>" id="input-employeeid">
                              <div id="error"></div>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <input type="text" class="form-control" name="lname" value="<?php echo $editemp->lname ;?>" id="input-lname">
                              <div id="error"></div>
                           </div>
                        </div>
                     </div>
                       <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                              	<label class="radio-inline">
            							      <input type="radio" name="gender" value="male" <?php echo ($editemp->gender=='male')?'checked':'' ?>>Male
            							     </label>     
            							       <label class="radio-inline">
            							      <input type="radio" name="gender" value="female" <?php echo ($editemp->gender=='female')?'checked':'' ?>>Female
            							     </label>
                              <div id="error"></div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                              <input type="text" class="form-control" name="username" value="<?php echo $editemp->username ;?>" id="input-username">
                              <div id="error"></div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <input type="text" class="form-control" name="email" value="<?php echo $editemp->email ;?>" id="input-email">
                              <div id="error"></div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <input type="text" class="form-control"  name="empdob" value="<?php echo $editemp->empdob ;?>" id="datepicker" placeholder="DD-MM-YY">
                              <div id="error"></div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <input type="submit" class="btn btn-block btn-dark" name="Update" value="Update">
                           </div>
                        </div>
                     </div>
                     <?php echo form_close();?>
                  </div>
               </div>
            </div>
         </div>
      </div>

    
      <script>
      	// set the calender format
		$( function() {
			$('input[id$=datepicker]').datepicker({
			dateFormat: 'dd-mm-yy'
			});
		} );

	  </script>
   </body>
</html>