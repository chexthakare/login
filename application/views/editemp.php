<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css');?>">
    <script src="<?php echo base_url('assets/js/jquery-1.12.4.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-ui.js');?>"></script>
</head>
<body>

<div id="container">
	<h1>Login to Enter</h1>

	<div id="body">
		<table>
			<?php echo form_open('welcome/update', array('id' => 'form2', 'role' => 'form'));?>
			<tr>
				<td>First Name</td><td><input type="text" name="fname" value="<?php echo $editemp->fname ;?>" id="input-fname">
        <input type="hidden" name="employeeid" value="<?php echo $editemp->employeeid ;?>" id="input-employeeid"></td>
			</tr>
			<tr>
				<td>Last Name</td><td><input type="text" name="lname" value="<?php echo $editemp->lname ;?>" id="input-lname"></td>
			</tr>
			<tr>
				<td>Email</td><td><input type="text" class="form-control" name="email" value="<?php echo $editemp->email ;?>" id="input-email"></td>
			</tr>
			<tr>
				<td>Birth Date</td><td><input type="text" name="empdob" value="<?php echo $editemp->empdob ;?>" id="datepicker" placeholder="DD-MM-YY"></td>
			</tr>
			<tr>
				<td>Gender</td><td><input type="radio" name="gender" value="male" <?php echo ($editemp->gender=='male')?'checked':'' ?>>Male <input type="radio" name="gender" value="female" <?php echo ($editemp->gender=='female')?'checked':'' ?>>Female</td>
			</tr>
			<tr>
				<td></td><td><input type="submit" name="submit" value="update"></td>
			</tr>
			<tr>
				<td></td><td><?php if($this->session->flashdata('message')){?>
			  	<div class="alert alert-danger" style="color:red;">      
			    <?php echo $this->session->flashdata('message')?>
			  	</div> <?php } ?></td>
			</tr>
			<?php echo form_close(); ?>
		</table>
	</div>

	<p class="footer">If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
</div>
<script>
      // set the calender format
      $( function() {
      $('input[id$=datepicker]').datepicker({
      dateFormat: 'dd-mm-yy'
      });
      } );
      
      /* Populate data to state dropdown */
      $('#country').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('welcome/getStates'); ?>',
                data:'country_id='+countryID,
                success:function(data){
                    $('#state').html('<option value="">Select State</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#state').append(option);
                        });
                    }else{
                        $('#state').html('<option value="">State not available</option>');
                    }
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
      });
      
      /* Populate data to city dropdown */
      $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('welcome/getCities'); ?>',
                data:'state_id='+stateID,
                success:function(data){
                    $('#city').html('<option value="">Select City</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#city').append(option);
                        });
                    }else{
                        $('#city').html('<option value="">City not available</option>');
                    }
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
      });
    </script>
</body>
</html>