<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
</head>
<body>

<div id="container">
	<h1>Login to Enter</h1>

	<div id="body">
		<table>
			<?php echo form_open('logcheck'); ?>
			<tr>
				<td>Usernme</td><td><input type="text" name="username"></td>
			</tr>
			<tr>
				<td>Password</td><td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td></td><td><input type="submit" name="submit" value="submit"></td>
			</tr>
			<tr>
				<td></td><td><?php if($this->session->flashdata('message')){?>
			  	<div class="alert alert-danger" style="color:red;">      
			    <?php echo $this->session->flashdata('message')?>
			  	</div> <?php } ?></td>
			</tr>
			<?php echo form_close(); ?>
		</table>
	</div>

	<p class="footer">If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
</div>

</body>
</html>