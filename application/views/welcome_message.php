<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css');?>">
    <script src="<?php echo base_url('assets/js/jquery-1.12.4.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-ui.js');?>"></script>
</head>
<body>

<div id="container">
	<h1>Login to Enter</h1>

	<div id="body">
		<table>
			<?php echo form_open('register', 'class="form" id="main"'); ?>
			<tr>
				<td>First Name</td><td><input type="text" name="fname"><div id="fname-error"></div></td>
			</tr>
			<tr>
				<td>Last Name</td><td><input type="text" name="lname"></td>
			</tr>
			<tr>
				<td>Email</td><td><input type="email" name="email"></td>
			</tr>
			<tr>
				<td>Birth Date</td><td><input type="text" name="empdob" id="datepicker"></td>
			</tr>
			<tr>
				<td>Gender</td><td><input type="radio" name="gender" value="male" checked="checked">Male <input type="radio" name="gender" value="female" >Female</td>
			</tr>
			<tr>
				<td>Country</td>
				<td>
					<select id="country" name="country" class="form-control">
                      <option value="">Select Country</option>
                      <?php
                        if(!empty($countries)){
                            foreach($countries as $row){ 
                                echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                            }
                        }else{
                            echo '<option value="">Country not available</option>';
                        }
                        ?>
                    </select>
				</td>
			</tr>
			<tr>
				<td>Country</td>
				<td>
					<select id="state" name="state" class="form-control">
                      <option value="">Select state first</option>
                    </select>
				</td>
			</tr>
			<tr>
				<td>Country</td>
				<td>
					<select id="city" name="city" class="form-control">
                      <option value="">Select city first</option>
                    </select>
				</td>
			</tr>
			<tr>
				<td>Usernme</td><td><input type="text" name="username"></td>
			</tr>
			<tr>
				<td>Password</td><td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td></td><td><input type="submit" name="submit" value="submit"></td>
			</tr>
			<tr>
				<td></td><td><?php if($this->session->flashdata('message')){?>
			  	<div class="alert alert-danger" style="color:red;">      
			    <?php echo $this->session->flashdata('message')?>
			  	</div> <?php } ?></td>
			</tr>
			<?php echo form_close(); ?>
		</table>
	</div>

	<p class="footer">If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
</div>
<script>
      // set the calender format
      $( function() {
      $('input[id$=datepicker]').datepicker({
      dateFormat: 'dd-mm-yy'
      });
      } );
      
      /* Populate data to state dropdown */
      $('#country').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('welcome/getStates'); ?>',
                data:'country_id='+countryID,
                success:function(data){
                    $('#state').html('<option value="">Select State</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#state').append(option);
                        });
                    }else{
                        $('#state').html('<option value="">State not available</option>');
                    }
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
      });
      
      /* Populate data to city dropdown */
      $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('welcome/getCities'); ?>',
                data:'state_id='+stateID,
                success:function(data){
                    $('#city').html('<option value="">Select City</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#city').append(option);
                        });
                    }else{
                        $('#city').html('<option value="">City not available</option>');
                    }
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
      });
    </script>
    <script>
	function validateFormOnSubmit(contact) {
	    reason = "";
	    reason += validateFname(contact.fname);
	    reason += validateLname(contact.lname);
	    reason += validateAddress(contact.address);
	    reason += validateEmail(contact.email);
	    reason += validatePhone(contact.phone);

	    console.log(reason);
	    if (reason.length > 0) {

	        return false;
	    } else {
	        return false;
	    }
	}

	// validate required first name fields
	function validateFname(fname) {
	    var error = "";

	    if (fname.value.length == 0) {
	        fname.style.background = 'Yellow';
	        document.getElementById('fname-error').innerHTML = "First Name required field has not been filled in";
	        var error = "1";
	    } else {
	        fname.style.background = 'White';
	        document.getElementById('fname-error').innerHTML = '';
	    }
	    return error;
	}

	// validate required last name fields
	function validateLname(lname) {
	    var error = "";

	    if (lname.value.length == 0) {
	        lname.style.background = 'Yellow';
	        document.getElementById('lname-error').innerHTML = "Last Name required field has not been filled in";
	        var error = "1";
	    } else {
	        lname.style.background = 'White';
	        document.getElementById('lname-error').innerHTML = '';
	    }
	    return error;
	}

	// validate required Address fields
	function validateAddress(address) {
	    var error = "";

	    if (address.value.length == 0) {
	        address.style.background = 'Yellow';
	        document.getElementById('address-error').innerHTML = "Address required field has not been filled in";
	        var error = "1";
	    } else {
	        address.style.background = 'White';
	        document.getElementById('address-error').innerHTML = '';
	    }
	    return error;
	}
	// validate email as required field and format
	function trim(s) {
	    return s.replace(/^\s+|\s+$/, '');
	}

	function validateEmail(email) {
	    var error = "";
	    var temail = trim(email.value); // value of field with whitespace trimmed off
	    var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/;
	    var illegalChars = /[\(\)\<\>\,\;\:\\\"\[\]]/;

	    if (email.value == "") {
	        email.style.background = 'Yellow';
	        document.getElementById('email-error').innerHTML = "Please enter an email address.";
	        var error = "2";
	    } else if (!emailFilter.test(temail)) { //test email for illegal characters
	        email.style.background = 'Yellow';
	        document.getElementById('email-error').innerHTML = "Please enter a valid email address.";
	        var error = "3";
	    } else if (email.value.match(illegalChars)) {
	        email.style.background = 'Yellow';
	        var error = "4";
	        document.getElementById('email-error').innerHTML = "Email contains invalid characters.";
	    } else {
	        email.style.background = 'White';
	        document.getElementById('email-error').innerHTML = '';
	    }
	    return error;
	}

	// validate phone for required and format
	function validatePhone(phone) {
	    var error = "";
	    var stripped = phone.value.replace(/[\(\)\.\-\ ]/g, '');

	    if (phone.value == "") {
	        document.getElementById('test').innerHTML = "Please enter a phone number";
	        phone.style.background = 'Yellow';
	        var error = '6';
	    } else if (isNaN(parseInt(stripped))) {
	        var error = "5";
	        document.getElementById('test').innerHTML = "The phone number contains illegal characters.";
	        phone.style.background = 'Yellow';
	    } else if (stripped.length < 10) {
	        var error = "6";
	        document.getElementById('test').innerHTML = "The phone number is too short.";
	        phone.style.background = 'Yellow';
	    } else {
	        phone.style.background = 'White';
	        document.getElementById('test').innerHTML = '';
	    }
	    return error;
	}
	</script>
</body>
</html>